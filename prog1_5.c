#include <stdio.h>
#include <string.h>
#define TRUE 1
#define FALSE 0


/** Method will check to see if the string contains anything besides digits 
if it does it returns false */

int is_integer(char* c ) {

        for(int i = 0; i < strlen(c); i++) {

                if((c[i] < '0' || c[i] > '9'))
                        return FALSE;

        }

        return TRUE;

}


/**
Method that will check if the input was in the correct format
*/
int is_correct(char* input){


        //loop through the input string and print a new line for each space 


        int start = 0;
        int end = 0;
        int num_token =0;

         //remove any leading 0's 
        while(start < 65 && input[start] == ' '){

                start ++;


        }
        end = start;

         //remove any tailing 0's 

        int max = (int) strlen(input) -1;

        while( max > 0 && (input[max] == ' ')) {
                max--;

        }

        //loop through and find the deliminated sections 

        while(end < max) {

                if(input[end] == ' ' || (end == (max-1 ))) {
                        int leng = end-start;
                        char sub[leng+1];

                        num_token++;

                        if(end == (max -1)) {

                                end++;

                        }

                        while(input[end] == ' ' && end< max) {
                               end++;
                               start = end;
                        }


                }

                else
                        end++;
        }

        return num_token;
}

/**
Method that will print the input in the correct format either str or INT
*/
int print_input(char* input, int num_token) {

        int start =0;
        int end =0;
	int current_token = 1;
	char token1[65];
	char token2[65];

         //remove any leading 0's 
        while(start < 65 && input[start] == ' '){

                start ++;


        }
        end = start;

        //remove any tailing 0's 

        int max = (int) strlen(input) -1;

        while( max > 0 && input[max] == ' ') {
                max--;

        }

        //loop through and find the deliminated sections         
         while(end < max) {

                if(input[end] == ' ' || (end == (max-1 ))){

                        int leng = end-start;
                        char sub[leng+1];
			


                                if(input[end] == ' ') {
					input[end] = '\0';
                                        fflush(NULL);
                                        strcpy(sub, &input[start]);
                                }

                                else {
					input[end +1] = '\0';
                                        fflush(NULL);
                                        strcpy(sub, &input[start]);

                                }

                                if(end == (max-1)) {
                                        end++;

                                }

                                while((input[end] == ' '|| input[end] == '\0') && end< max) {
                                        end++;
                                        start = end;
                                }

                               

				//check to see which token we are on and save that to the correct place

				if(current_token ==1) {

					fflush(NULL);
					strcpy(token1, sub);

				}

				else if(current_token == 2) {
				
					fflush(NULL);
					strcpy(token2, sub);
				
				}

                                
					
                                memset(sub, '\0', strlen(sub));
				fflush(NULL);
				current_token++;
                }
                else
                        end++;
        }

	
	//at this point both the tokens should be stored in token 1 and token2, or just token 1

	//handel if there was supposed to be only 1 token

	if(num_token ==1) {

		if (is_integer(token1) == TRUE) {

			printf("ERROR! Expected STR.\n");

		}
		
		else
			printf("STR\n");
	
	}

	else {
		//Check to see if it is in the right format for two tokens
		if(is_integer(token1) ==FALSE && is_integer(token2) == TRUE) {

			printf("STR INT\n");

		}

		else {

			printf("ERROR! Expected STR INT.\n");
		}

	}

        return 0;


}

/**
Checks if the user entered QUIT to terminate the program
*/
int is_done(char* input) {

        //if it is the wrong number of charecters it cant be true
        if(strlen(input) != 5)
                return FALSE;

        //now check if each charecter is correct 

        char check[4] = "quiT";

        for (int i=0; i<4 ; i++) {

                if (input[i] != check[i])
                        return FALSE;

        }

        return TRUE;


}


int main( int argc, const char* argv[] )
{

        printf( "Assignment #1-5, Julia Cassella, juliarc@sandiego.edu\n" );

        char input[65];

        printf("> ");

        fgets( input, 65, stdin);

        while(is_done(input) == FALSE) {



        //loop through the input string and print a new line for each space 


        int start = 0;
        int end = 0;
        int num_token = is_correct(input);

	while(input[0] == '\n') {
		 printf("ERROR! Incorrect number of tokens found.\n");
		 printf("> ");
                //clear the memory for input 
                memset(input, '\0', 65);

                //read in a new input 

                fgets( input, 65 , stdin);


	}

	num_token = is_correct(input);
	//loops until there are the correct number of tokens and until the string is the right size
        while(num_token >2 || strlen(input) > 65) {

                 if(strlen(input) > 65) {

                        printf("ERROR! Input string too long.\n");

                }

                else {

                        printf("ERROR! Incorrect number of tokens found.\n");
                }
                printf("> ");
                //clear the memory for input 
                memset(input, '\0', 65);

                //read in a new input 

                fgets( input, 65 , stdin);


		   while(input[0] == '\n') {
			 printf("ERROR! Incorrect number of tokens found.\n");
               		  printf("> ");
        	        //clear the memory for input 
                	memset(input, '\0', 65);

        	        //read in a new input 

        	        fgets( input, 65 , stdin);


       		 }
		
                num_token = is_correct(input);

      }


        //so there must be at most two tokens 

         print_input(input, num_token);



        printf("> ");
                //clear the memory for input 
                memset(input, '\0', 65);

                //read in a new input 

                fgets(input, 65, stdin);
		
		 while(input[0] == '\n') {
			 printf("ERROR! Incorrect number of tokens found.\n");
                          printf("> ");
                        //clear the memory for input 
                        memset(input, '\0', 65);

                        //read in a new input 

                        fgets( input, 65 , stdin);


                 }
	num_token = is_correct(input);

}

        return 0;

}


