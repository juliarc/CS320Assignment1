Assignment 1 Julia Cassella juliarc@sandiego.edu

The point of the overall project was to become comfortable with the STDIN and STDOUT areas of C. This was done by asking for a specified length input from the user, deliminating it by spaces and doing different desired steps for each of them. Each consecutive project built off of the previous projects 


Program 1: This program took an input from the user that could be no longer than 65 char, I did this using scanf() that was deliminated by the new line charecter. Then I looped through the input and found each word (when there was a space to end it or if it was the last letter in the array) and then each word was printed in the format =word=

Program 2: Like program 1 the input was read in from the user and delinminated by spaces, but then instead of printing off in the format =word= I passed each word through an is_integer method that checked to see if all the chars in the word were ints, if they were then INT was printed off, otherwise STR was printed off.

Program 3: Exactly like program two except there could only be at most two tokens input by the user. if there were more than two the prgram would promt the user to try again until there were at most two put in. These two were then printed in the same way as program 2

Program 4: Like program 3 with the modification that the input could only be at most 20 charecters, if it was longer an error would be printed and the user would be asked to try again. If too many tokens wree entered an error would be printed and the user would have to try again. And even if the right number of tokens were entered the program would not terminate until the user input "QuIt"

Program 5: Like program 4 except the user can input up to 65 charecters. Also the tokens had to be a certain type. If there was one token it had to be a STR and if there were two it had to be a STR followed by an INT. If these were put in wrong the program would promt the user to try again until the correct formatting was put in. The program would only terminate is the user put in "QUIT"

Prgram 6: Exaclty like program 5 except there is an additional command line argument that contains the max number of attempts. If the command arguments were in the wrong format and error would be printed and the program would terminate. All other behaviour would be like program 5 except the program would also terminate once we have entered as many inputs as the number in the command line specified. 
