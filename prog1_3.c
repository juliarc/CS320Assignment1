#include <stdio.h>
#include <string.h>
#include <ctype.h>
#define TRUE 1
#define FALSE 0



/** Method will check to see if the string contains anything besides digits 
if it does it returns false */

int is_integer(char* c ) {

        for(int i = 0; i < strlen(c); i++) {

               if( isdigit(c[i])==FALSE && c[i] != '\n')
		return FALSE;

        }

        return TRUE;

}


/**
This method will deliminate by spaces and count the number of tokens 
The number of tokens is then returned
*/
int is_correct(char* input){


        //loop through the input string and print a new line for each space 


        int start = 0;
        int end = 0;
	int num_token =0;
	
         //remove any leading 0's 
        while(start < 65 && input[start] == ' '){

                start ++;


        }
        end = start;

        //remove any tailing 0's 

        int max = (int) strlen(input) -1;

        while( max > 0 && (input[max] == ' '|| input[max] == '\n')) {
                max--;

        }
	
	//loop through and find the deliminated sections 

        while(end < max) {
		//So we have a word
                if(input[end] == ' ' || end == (max-1)) {
                        int leng = end-start;
                        
			//increment the number of tokens 
			num_token++;			
	
			if(end == (max -1)) {

				end++;

			}
			//get rid of inbetween white spaces
			while(input[end] == ' ' && end< max) {
                               end++;
                               start = end;
                        }


                }

                else
                        end++;
        }

	return num_token;
}

/**
This method will print the foramt of the input, so whether it is a string of a int
*/
int print_input(char* input) {



        int start =0;
        int end =0;
         //remove any leading 0's 
        while(start < 65 && input[start] == ' '){

                start ++;


        }
        end = start;

        //remove any tailing 0's 

        int max = (int) strlen(input) -1;

        while( max > 0 && input[max] == ' ') {
                max--;

        }

        //loop through and find the deliminated sections         
         while(end < max) {
		
		//We have a word
                if(input[end] == ' ' || end == (max-1)){

                        int leng = end-start;
                        char sub[leng+1];
				
				//coppies over the sub string with their necesary lengths
                                if(input[end] == ' ') {
					fflush(NULL);
                                        memcpy(sub, &input[start], (leng));
				}

                                else {
					fflush(NULL);
                                        memcpy(sub, &input[start], (leng+1));

                                }

                                if(end == (max-1)) {
                                        end++;

                                }
				
				//Gets rid of inbetween white spaces
                                while(input[end] == ' ' && end< max) {
                                        end++;
                                        start = end;
                                }

                               //pass sub through the is_integer method if it returns false print string else print int

                                if(is_integer(sub) == TRUE) {

                                       printf("INT ");

                                }


                                else
                                        printf("STR ");

                                memset(sub, '\0', strlen(sub));
                }
                else
                        end++;
        }
printf("\n");
return 0;
}




int main( int argc, const char* argv[] )
{

	printf( "Assignment #1-3, Julia Cassella, juliarc@sandiego.edu\n" );

        char input[65];

        printf("> ");

        fgets( input,65,stdin);
	fflush(NULL);
        // check to see if the correct number of tokems 
        int num_token = is_correct(input);

	while(input[0] == '\n') {
		
		printf("ERROR! Incorrect number of tokens found.\n");
		printf("> ");
                //clear the memory for input 
                memset(input, '\0', 65); 

                //read in a new input 
		
		fflush(NULL);
                fgets(input, 65, stdin);
		
                

	}

	num_token = is_correct(input);

	//while we have too many tokens 
	while(num_token >2) {

	printf("ERROR! Incorrect number of tokens found.\n");
	printf("> ");
		//clear the memory for input 
		memset(input, '\0', 65);		

		//read in a new input 
		fflush(NULL);
		fgets(input, 65, stdin);
		
		    while(input[0] == '\n') {

                printf("ERROR! Incorrect number of tokens found.\n");
                printf("> ");
                //clear the memory for input 
                memset(input, '\0', 65);

                //read in a new input 
		fflush(NULL);
                fgets(input, 65, stdin);
		
        }

		num_token = is_correct(input);

	}

	//so there must be at most two tokens

	print_input(input);
	return 0;

}
