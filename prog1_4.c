#include <stdio.h>
#include <string.h>
#include <ctype.h>
#define TRUE 1
#define FALSE 0


/** Method will check to see if the string contains anything besides digits 
if it does it returns false */

int is_integer(char* c ) {

        for(int i = 0; i < strlen(c); i++) {

                if(c[i] < '0' || c[i] > '9')
                        return FALSE;

        }

        return TRUE;

}


/**
MEthod will test if thre are the correct number of tokens and will then return those tokens 
*/
int is_correct(char* input){


        //loop through the input string and print a new line for each space 

	//check if the the input was empty 

	
        int start = 0;
        int end = 0;
        int num_token =0;

         //remove any leading 0's 
        while(start < 65 && input[start] == ' '){

                start ++;


        }
        end = start;
		
	 //remove any tailing 0's 

        int max = (int) strlen(input) -1;

        while( max > 0 && (input[max] == ' ' )) {
                max--;

        }

        //loop through and find the deliminated sections 

        while(end < max) {

		//So we have a word   
              if(input[end] == ' ' || (end == (max-1 ))) {
                        int leng = end-start;
                        char sub[leng+1];

                        num_token++;

                        if(end == (max -1)) {

                                end++;

                        }

                        while(input[end] == ' ' && end< max) {
                               end++;
                               start = end;
                        }


                }

                else
                        end++;
        }

        return num_token;
}

/**
This will print the input in the correct format, either STR or INT
*/
int print_input(char* input) {

	 int start =0;
        int end =0;
         //remove any leading 0's 
        while(start < 65 && input[start] == ' '){

                start ++;


        }
        end = start;

        //remove any tailing 0's 

        int max = (int) strlen(input) -1;

        while( max > 0 && (input[max] == ' ')) {
                max--; 

        }

        //loop through and find the deliminated sections         
         while(end < max) {
		//So we have a word
                if(input[end] == ' ' || (end == (max-1 ))){

                        int leng = end-start;
                        char sub[leng+2];
			
			if(input[end] == ' ') {
                        	input[end] = '\0';
                                fflush(NULL); 
                                strcpy(sub, &input[start]);
               		}

			else {
				input[end+1] ='\0';
				fflush(NULL);
				strcpy(sub, &input[start]);

			}

                                if(end == (max-1)) {
                                        end++;

                                }

                                while((input[end] == ' ' || input[end] == '\0') && end< max) {
                                        end++;
                                        start = end;
                                }

                               //pass sub through the is_integer method if it returns false print string else print int

                                if(is_integer(sub) == TRUE) {

                                        printf("INT ");

                                }


                                else
                                        printf("STR ");

                                memset(sub, '\0', strlen(sub));
                }
                else
                        end++;
        }
	printf("\n");
	return 0;


}

/**
Method that will check if the user input "QuIt"
*/
int is_done(char* input) {

	//if it is the wrong number of charecters it cant be true
	if(strlen(input) != 5) 
		return FALSE;

	//now check if each charecter is correct 

	char check[4] = "qUit";

	for (int i=0; i<4 ; i++) {

		if (input[i] != check[i])
			return FALSE; 
	
	}

	return TRUE;


}
        
int main( int argc, const char* argv[] )
{

        printf( "Assignment #1-4, Julia Cassella, juliarc@sandiego.edu\n" );

        char input[65];
	int num_token;

        printf("> ");

        fgets( input, 65, stdin);

	while(is_done(input) == FALSE) {

	//check to see if we are dealing with a new line charecter

	if(input[0] == '\n') {

		 printf("ERROR! Incorrect number of tokens found.\n");
		printf("> ");

              

                
                //clear the memory for input
                fflush(NULL);
                memset(input, '\0', 65);

                //read in a new input 
		 fgets(input, 65, stdin);
		fflush(NULL);

                num_token = is_correct(input);


	}
	
        //loop through the input string and print a new line for each space 


        int start = 0;
        int end = 0;
        num_token = is_correct(input);

	

        while(num_token >2 || strlen(input) > 21) {

      		 if(strlen(input) > 21) {

			printf("ERROR! Input string too long.\n");

		}

		else {
	
			printf("ERROR! Incorrect number of tokens found.\n");
	      	}	
		printf("> ");

		
		//clear the memory for input
		fflush(NULL); 
      		memset(input, '\0', 65);

        	//read in a new input 

		fgets( input, 65, stdin);
		fflush(NULL);

		//check to see if we are dealing with a new line charecter

        if(input[0] == '\n') {

                 printf("ERROR! Incorrect number of tokens found. \n");
                printf("> ");

                
                //clear the memory for input
                fflush(NULL);
                memset(input, '\0', 65);

                //read in a new input 
                 fgets(input, 65, stdin);
		fflush(NULL);

                num_token = is_correct(input);


        }


	        num_token = is_correct(input);

      }
	

        //so there must be at most two tokens 

         print_input(input);
     


	printf("> ");

                //clear the memory for input 
                memset(input, '\0', 65);

                //read in a new input 

                fgets( input, 65, stdin);
	
}

        return 0;

}
                             
