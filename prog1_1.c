#include <stdio.h>
#include <string.h>



/**
This program reads in a string from the user, deliminated it by spaces and prints off in the form =word1= =word2=....
*/


int main( int argc, const char* argv[] )
{

	printf( "Assignment #1-1, Julia Cassella, juliarc@sandiego.edu\n" );

	char input[65];

	scanf("%[^\n]%*c", input);

	//loop through the input string and print a new line for each space 
	
		

	int start = 0;
	int end = 0;

	//remove any leading 0's 
	while(start < 65 && input[start] == ' '){
		
		start ++;

	
	}
	end = start;

	//remove any tailing 0's 

	int max = (int) strlen(input); 

	while( max > 0 && input[max] == ' ') {
		max--;
	}


	//Scan through the input
	while(end < max) { 
		
		//this means we have reached the end of a single word that spans start to end 
		if(input[end] == ' ' || (end == (max -1))) { 
		
			int leng = end-start;
			char sub[leng+1];	
		
				if(input[end] == ' ' || end == max) {	
					
					memcpy(sub, &input[start], (leng));			
			
				}
			
				else {	
					memcpy(sub, &input[start], (leng+1));
				
				}

				//if you are currently looking at the last charecter 

				if(end == (max -1)) {

				end++;
				}				

				//skip over all the inbetween white spaces to get to the next start of the word	
				while(input[end] == ' ' && end< max) {


					end++; 

					start = end;
				}
			
			
				
				printf("=%s=\n",sub);				
	
				memset(sub, '\0', leng);
			
				
		}
			

		else {
			end++;
		}
	}



	return 0;




}
