#include <stdio.h>
#include <string.h>
#define TRUE 1
#define FALSE 0

/**
This prog will print if the delinated words are ints or STR */

/** Method will check to see if the string contains anything besides digits 
if it does it returns false */

int is_integer(char* c ) {

	//loops through each char and sees if it is an int, any arent ints then the input was a STR
        for(int i = 0; i < strlen(c); i++) {
	
		if(!(c[i] >= '0' && c[i] <= '9'))
			return FALSE;
      
        }

        return TRUE;

}

// Main method
int main( int argc, const char* argv[] )
{

        printf( "Assignment #1-2, Julia Cassella, juliarc@sandiego.edu\n" );

        char input[65];

	printf("> ");

        scanf("%[^\n]%*c", input);

        //loop through the input string and print a new line for each space 


        int start = 0;
        int end = 0;

	 //remove any leading 0's 
        while(start < 65 && input[start] == ' '){

                start ++;


        }
        end = start;

        //remove any tailing 0's 

        int max = (int) strlen(input);

        while( max > 0 && input[max] == ' ') {
                max--;
        
	}

	//loop through and find the deliminated sections 

	while(end < max) {

              	//We are looking at a word
		  if(input[end] == ' ' || (end == (max-1 ))){

                        int leng = end-start;
                        char sub[leng+1];

                        	if(input[end] == ' ') {

                                        memcpy(sub, &input[start], (leng));

                                }

				//If the ending charecter was the last charecter in the input we need to make sure the length is one more
				//This is becasue the value of end is still an index in the word
                                else {

                                        memcpy(sub, &input[start], (leng+1));

                                }
              
				if(end == (max-1)) {
					end++;

				}				

                                while(input[end] == ' ' && end< max) {
                                        end++;
                                        start = end;
                                }

                               //pass sub through the is_integer method if it returns false print string else print int
				
				if(is_integer(sub) == TRUE) {
					
					printf("INT ");
				}

				else 
					printf("STR ");

                                memset(sub, '\0', leng);
		}
                else
                        end++;
        }

	printf("\n");
        return 0;


	
}


